﻿using System;

namespace H5OefeningenDeel1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Oefening 9
            int getal = -100;
            while (getal <= 100)
            {
                Console.WriteLine(getal);
                getal++;
            }

            //Oefening 11
            for (int i = -100; i <= 100; i++)
            {
                Console.WriteLine(i);
            }

            //Oefening 13
            for (int i = 1; i < 10; i++)
            {
                if (i % 6 == 0 || i % 8 == 0)
                {
                    Console.WriteLine(i);
                }
            }

            //Euler-project
            int som = 0;
            for (int i = 1; i <= 1000; i++)
            {
                if (i % 3 == 0 || i % 5 == 0)
                {
                    som += i;
                }
            }
            Console.WriteLine(som);

            //For-doordenker
            Console.Write("Geef een getal: ");
            int getal = Convert.ToInt32(Console.ReadLine());

            for (int j = 0; j < getal; j++)
            {
                for (int i = 0; i < j; i++)
                {
                    Console.Write("*");
                }
                Console.WriteLine();
            }
            for (int j = 0; j < getal; j++)
            {
                for (int i = j; i < getal; i++)
                {
                    Console.Write("*");
                }
                Console.WriteLine();
            }

            //For-doordenker extra
            Console.Write("Geef een getal: ");
            int getal = Convert.ToInt32(Console.ReadLine());

            for (int i = 1; i <= getal; i++)
            {
                for (int j = 1; j < getal - i + 1; j++)
                {
                    Console.Write(" ");
                }
                for (int k = 1; k <= i; k++)
                {
                    Console.Write("*");
                    Console.Write(" ");
                }
                Console.WriteLine();
            }
        }
    }
}
