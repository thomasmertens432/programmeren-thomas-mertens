﻿using System;

namespace tafels_van_vermenigvuldigen
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int j = 1; j < 11; j++)
            {
                for (int i = 1; i < 11; i++)
                {
                    int uitkomst = j * i;
                    Console.WriteLine(j + "x" + i + " = " + uitkomst);
                } 
            }
        }
    }    
}
