﻿using System;

namespace Armstrong_nummer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Geef een getal: ");
            int getal1 = Convert.ToInt32(Console.ReadLine());

            //Eerst deel je 4563 door 1000.Dit geeft 4.
            int getal2 = getal1 / 1000;

            //We trekken 4x1000 van 4563 af.Dit geeft 563.
            int getal3 = getal1-(getal2 * 1000);
            
            //Deel 563 door 100.Dit geeft 5.
            int getal4 = getal3 / 100;

            //We trekken 5x100 van 563 af.Dit geeft 63.
            int getal5 = getal3 - (getal4 * 100);

            //Deel 63 door 10.Dit geeft 6.
            int getal6 = getal5 / 10;

            //We trekken 6 x 10 van 63 af.Dit geeft 3
            int getal7 = getal5 - (getal6 * 10);

            int lengte = getal1.ToString().Length;
            double expo = Math.Pow(10, lengte - 1);

            int uitkomst = (getal2 ^ (int)expo) + (getal4 ^ (int)expo) + (getal6 ^ (int)expo) + (getal7 ^ (int)expo);

            Console.WriteLine(uitkomst);

        }
    }
}
