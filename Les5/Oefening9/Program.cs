﻿using System;

namespace Oefening9
{
    class Program
    {
        static void Main(string[] args)
        {
            int getal = -100;
            while (getal <= 100)
            {
                Console.WriteLine(getal);
                getal++;
            }
        }
    }
}
