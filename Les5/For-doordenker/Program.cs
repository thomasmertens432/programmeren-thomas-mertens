﻿using System;

namespace For_doordenker
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Geef een getal: ");
            int getal = Convert.ToInt32(Console.ReadLine());

            for (int j = 0; j < getal; j++)
            {
                for (int i = 0; i < j; i++)
                {
                    Console.Write("*");
                }
                Console.WriteLine();
            }
            for (int j = 0; j < getal; j++)
            {
                for (int i = j; i < getal; i++)
                {
                    Console.Write("*");
                }
                Console.WriteLine();
            }


        }
    }
}
