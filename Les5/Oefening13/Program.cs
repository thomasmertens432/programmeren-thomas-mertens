﻿using System;

namespace Oefening13
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 1; i < 10; i++)
            {
                if (i % 6 == 0 || i % 8 == 0)
                {
                    Console.WriteLine(i);
                }
            }
        }
    }
}
