﻿using System;

namespace MyTutorialProject
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.BackgroundColor = ConsoleColor.White;
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Magenta;
            Boolean bools = true;
            string kleur;
            Console.WriteLine("Geslacht (M/V): ");
            string geslacht = Console.ReadLine();

            do
            {
                Console.ForegroundColor = ConsoleColor.Green;
                
                if (geslacht == "M")
                {
                    Console.WriteLine("Wat is je favoriete kleur meneer? ");
                    kleur = Console.ReadLine();
                    bools = false;
                }
                else if (geslacht == "V")
                {
                    Console.WriteLine("Wat is je favoriete kleur mevrouw? ");
                    kleur = Console.ReadLine();
                    bools = false;
                }
                else
                {
                    Console.WriteLine("Oeps je hebt iets verkeerds ingetyped probeer opnieuw.");
                }
            } while (bools);

            kleur = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Wat is je favoriete eten? ");
            string eten = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Wat is je favoriete auto? ");
            string auto = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Wat is je favoriete film? ");
            string film = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Wat is je favoriete boek? ");
            string boek = Console.ReadLine();

            Console.WriteLine("Je favoriete kleur is " + kleur + ". Je eet graag " + eten + ". Je lievelingsfilm is " + film + " en je favoriete boek is " + boek + ".");


        }
    }
}