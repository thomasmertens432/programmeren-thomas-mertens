﻿using System;

namespace Oefening7
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Geef een getal: ");
            int getal = Convert.ToInt32(Console.ReadLine());

            for (int i=1; i<getal; i++)
            {
                if (i % 2 != 0)
                {
                    Console.Write(i + " + ");
                }
            }
            Console.WriteLine(getal);
        }
    }
}
