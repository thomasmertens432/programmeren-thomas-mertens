﻿using System;

namespace Oefening10
{
    class Program
    {
        static void Main(string[] args)
        {
            //A-Z = 65-90
            //a-z = 97-122

            Console.WriteLine("Getal boven 10: ");
            int getal = Convert.ToInt32(Console.ReadLine());
            getal += 65;
            int c = 65;
            int d = 10;

            do
            {
                char e = Convert.ToChar(c);
                Console.WriteLine(e + ": " + d);
                c++;
                d++;
            } while (c <= getal);
        }
    }
}
