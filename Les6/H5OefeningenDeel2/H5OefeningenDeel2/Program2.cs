﻿using System;

namespace H5OefeningenDeel2
{
    class Program
    {
        static void Main(string[] args)
        {
            //Oefening 1
            Console.Write("Geef een getal: ");
            int getal = Convert.ToInt32(Console.ReadLine());

            for (int i = 1; i <= getal; i++)
            {
                Console.WriteLine(i);
            }

            //Oefening 2
            Console.Write("Geef een geheel getal kleiner dan 1: ");
            int getal = Convert.ToInt32(Console.ReadLine());

            do
            {
                Console.WriteLine(getal);
                getal++;
            } while (getal <= 1);

            //Oefening 3
            int getal = 1;

            while (getal <= 100)
            {
                if (getal % 2 == 0)
                {
                    Console.Write(getal + " ");
                }
                getal++;
            }

            //Oefening 4
            for (int i = 1; i <= 100; i++)
            {
                if (i % 2 != 0)
                {
                    Console.Write(i + " ");
                }
            }

            //Oefening 5
            Console.Write("Geef een getal: ");
            int getal = Convert.ToInt32(Console.ReadLine());
            int i = 1;

            do
            {
                Console.Write(i + " + ");
                i++;
            } while (i < getal);
            Console.Write(getal);

            //Oefening 6
            Console.Write("Geef een getal: ");
            int getal = Convert.ToInt32(Console.ReadLine());
            int i = 1;

            while (i < getal)
            {
                if (i % 2 == 0)
                {
                    Console.Write(i + " + ");
                }
                i++;
            }
            Console.WriteLine(getal);

            //Oefening 7
            Console.Write("Geef een getal: ");
            int getal = Convert.ToInt32(Console.ReadLine());

            for (int i = 1; i < getal; i++)
            {
                if (i % 2 != 0)
                {
                    Console.Write(i + " + ");
                }
            }
            Console.WriteLine(getal);

            //Oefening 8
            Console.Write("Geef een getal: ");
            int getal = Convert.ToInt32(Console.ReadLine());
            int count = 0;

            do
            {
                getal = getal / 10;
                count++;

            } while (getal > 0);

            Console.WriteLine("Dit getal heeft " + count + " digits.");

            //Oefening 10
            Console.WriteLine("Getal boven 10: ");
            int getal = Convert.ToInt32(Console.ReadLine());
            getal += 65;
            int c = 65;
            int d = 10;

            do
            {
                char e = Convert.ToChar(c);
                Console.WriteLine(e + ": " + d);
                c++;
                d++;
            } while (c <= getal);

            //Oefening 11
            for (int i = 97; i <= 122; i++)
            {
                char c = Convert.ToChar(i);
                Console.WriteLine(c);
            }

            //Priem-Checker
            Console.Write("Geef een getal: ");
            int getal = Convert.ToInt32(Console.ReadLine());
            int a = 0;
            for (int i = 1; i <= getal; i++)
            {
                if (getal % i == 0)
                {
                    a++;
                }
            }
            if (a == 2)
            {
                Console.WriteLine("Dit is een priemgetal!");
            }
            else
            {
                Console.WriteLine("Dit is geen priemgetal!");
            }

            //Priem-Generator
            Console.Write("Geef een getal: ");
            int getal = Convert.ToInt32(Console.ReadLine());
            int a = 0;
            for (int j = 2; j <= getal; j++)
            {
                int i;
                for (i = 1; i <= j; i++)
                {
                    if (j % i == 0)
                    {
                        a++;
                    }
                }
                if (a == 2)
                {
                    Console.WriteLine(j);
                }
            }

        }
    }
}
