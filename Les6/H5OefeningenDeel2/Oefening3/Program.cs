﻿using System;

namespace Oefening3
{
    class Program
    {
        static void Main(string[] args)
        {
            int getal = 1;

            while (getal <= 100)
            {
                if (getal % 2 == 0)
                {
                    Console.Write(getal + " ");
                }
                getal++;
            }
        }
    }
}
