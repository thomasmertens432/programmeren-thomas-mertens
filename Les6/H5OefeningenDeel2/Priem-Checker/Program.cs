﻿using System;

namespace Priem_Checker
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Geef een getal: ");
            int getal = Convert.ToInt32(Console.ReadLine());
            int a = 0;
            for (int i = 1; i <= getal; i++)
            {
                if (getal% i == 0)
                {
                    a++;
                }
            }
            if (a == 2)
            {
                Console.WriteLine("Dit is een priemgetal!");
            }
            else
            {
                Console.WriteLine("Dit is geen priemgetal!");
            }
        }
    }
}
