﻿using System;

namespace Oefening8
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Geef een getal: ");
            int getal = Convert.ToInt32(Console.ReadLine());
            int count = 0;
            

            do
            {
                getal = getal / 10;
                count++;
                
            } while (getal > 0);

            Console.WriteLine("Dit getal heeft " + count + " digits.");
        }
    }
}
