﻿using System;

namespace Oefening2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Geef een geheel getal kleiner dan 1: ");
            int getal = Convert.ToInt32(Console.ReadLine());

            do
            {
                Console.WriteLine(getal);
                getal++;
            } while (getal <= 1);
        }
    }
}
