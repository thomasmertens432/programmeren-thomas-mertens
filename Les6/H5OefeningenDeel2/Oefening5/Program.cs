﻿using System;

namespace Oefening5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Geef een getal: ");
            int getal = Convert.ToInt32(Console.ReadLine());
            int i = 1;

            do
            {
                Console.Write(i + " + ");
                i++;
            } while (i < getal);
            Console.Write(getal);
        }
    }
}
