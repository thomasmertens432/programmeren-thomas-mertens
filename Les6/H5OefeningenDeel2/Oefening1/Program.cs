﻿using System;

namespace Oefening1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Geef een getal: ");
            int getal = Convert.ToInt32(Console.ReadLine());

            for(int i=1; i<=getal; i++)
            {
                Console.WriteLine(i);
            }
        }
    }
}
