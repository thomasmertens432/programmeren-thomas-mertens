﻿using System;

namespace Oefening6
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Geef een getal: ");
            int getal = Convert.ToInt32(Console.ReadLine());
            int i = 1;

            while (i<getal)
            {
                if (i % 2 == 0)
                {
                    Console.Write(i + " + ");
                }
                i++;
            }
            Console.WriteLine(getal);
        }
    }
}
