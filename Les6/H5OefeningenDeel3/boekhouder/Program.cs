﻿using System;

namespace boekhouder
{
    class Program
    {
        static void Main(string[] args)
        {
            int balans = 0, pos = 0, neg = 0, gem = 0,tel = 1;

            while(true)
            {
                Console.Write("Geef een getal: ");
                int getal = Convert.ToInt32(Console.ReadLine());
                
                //balans
                balans += getal;
                Console.WriteLine("De balans is " + balans);
                
                //positieve getallen
                if(getal >= 0)
                {
                    pos += getal;
                }
                Console.WriteLine("De som van de positieve getallen is " + pos);

                //negatieve getallen
                if (getal <= 0)
                {
                    neg += getal;
                }
                Console.WriteLine("De som van de negatieve getallen is " + neg);

                //gemiddelde
                gem = balans / tel;
                Console.WriteLine("Het gemiddelde is " + gem);
                tel++;
                
            }
        }
    }
}
