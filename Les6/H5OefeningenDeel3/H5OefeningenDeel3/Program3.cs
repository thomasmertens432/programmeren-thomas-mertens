﻿using System;

namespace H5OefeningenDeel3
{
    class Program
    {
        static void Main(string[] args)
        {
            //BOEKHOUDER
            int balans = 0, pos = 0, neg = 0, gem = 0, tel = 1;

            while (true)
            {
                Console.Write("Geef een getal: ");
                int getal = Convert.ToInt32(Console.ReadLine());

                //balans
                balans += getal;
                Console.WriteLine("De balans is " + balans);

                //positieve getallen
                if (getal >= 0)
                {
                    pos += getal;
                }
                Console.WriteLine("De som van de positieve getallen is " + pos);

                //negatieve getallen
                if (getal <= 0)
                {
                    neg += getal;
                }
                Console.WriteLine("De som van de negatieve getallen is " + neg);

                //gemiddelde
                gem = balans / tel;
                Console.WriteLine("Het gemiddelde is " + gem);
                tel++;

                //SCHAAR_STEEN_PAPIER
                int flag = 0;
                int userpunt = 0, pcpunt = 0;

                do
                {
                    Console.WriteLine("Maak een keuze");
                    Console.WriteLine("1 voor schaar");
                    Console.WriteLine("2 voor steen");
                    Console.WriteLine("3 voor papier");
                    Console.Write("> ");
                    int getal = Convert.ToInt32(Console.ReadLine());
                    Random random = new Random();
                    int pc = random.Next(1, 4);

                    if (getal == 1 && pc == 1)
                    {
                        Console.WriteLine("De computer kiest schaar!");
                        Console.WriteLine("Niemand wint deze ronde!");
                    }
                    else if (getal == 2 && pc == 2)
                    {
                        Console.WriteLine("De computer kiest steen!");
                        Console.WriteLine("Niemand wint deze ronde!");
                    }
                    else if (getal == 3 && pc == 3)
                    {
                        Console.WriteLine("De computer kiest papier!");
                        Console.WriteLine("Niemand wint deze ronde!");
                    }
                    else if (getal == 1 && pc == 2)
                    {
                        Console.WriteLine("De computer kiest steen!");
                        Console.WriteLine("De computer wint deze ronde!");
                        pcpunt++;
                    }
                    else if (getal == 1 && pc == 3)
                    {
                        Console.WriteLine("De computer kiest papier");
                        Console.WriteLine("Jij wint deze ronde!");
                        userpunt++;
                    }
                    else if (getal == 2 && pc == 1)
                    {
                        Console.WriteLine("De computer kiest schaar");
                        Console.WriteLine("Jij wint deze ronde!");
                        userpunt++;
                    }
                    else if (getal == 2 && pc == 3)
                    {
                        Console.WriteLine("De computer kiest papier");
                        Console.WriteLine("De computer wint deze ronde!");
                        pcpunt++;
                    }
                    else if (getal == 3 && pc == 1)
                    {
                        Console.WriteLine("De computer kiest schaar");
                        Console.WriteLine("De computer wint deze ronde!");
                        pcpunt++;
                    }
                    else if (getal == 3 && pc == 2)
                    {
                        Console.WriteLine("De computer kiest steen!");
                        Console.WriteLine("Jij wint deze ronde!");
                        userpunt++;
                    }

                    if (userpunt == 1 && pcpunt == 1)
                    {
                        Console.WriteLine("Jij hebt " + userpunt + " punt, de computer heeft " + pcpunt + " punt.");
                    }
                    else if (userpunt == 1)
                    {
                        Console.WriteLine("Jij hebt " + userpunt + " punt, de computer heeft " + pcpunt + " punten.");
                    }
                    else if (pcpunt == 1)
                    {
                        Console.WriteLine("Jij hebt " + userpunt + " punten, de computer heeft " + pcpunt + " punt.");
                    }
                    else
                    {
                        Console.WriteLine("Jij hebt " + userpunt + " punten, de computer heeft " + pcpunt + " punten.");
                    }

                    if (userpunt == 10 || pcpunt == 10)
                    {
                        flag = 1;
                    }
                } while (flag == 0);

                if (userpunt == 10)
                {
                    Console.WriteLine("Jij hebt gewonnen!");
                }
                else
                {
                    Console.WriteLine("De computer is gewonnen!");
                }

            }
        }
    }
}
