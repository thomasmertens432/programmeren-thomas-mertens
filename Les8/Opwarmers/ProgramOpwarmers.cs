﻿using System;

namespace Opwarmers
{
    class Program
    {
        static void Main(string[] args)
        {
            //Maak een array gevuld met de letters a tot z
            char[] alpha = "abcdefghijklmnopqrstuvwxyz".ToCharArray();

            //Maak een array gevuld met willekeurige getallen tussen 1 en 100 (de array is 20 lang)
            int[] getal = new int[20];
            Random randomG = new Random();
            for (int i = 0; i < 20; i++)
            {
                getal[i] = randomG.Next(1, 100);
            }

            //Maak een array gevuld met afwisselend true en false (de array is 30 lang)
            Boolean[] foo = new Boolean[30];
            for (int i = 0; i < foo.Length; i += 2)
            {
                foo[i+1] = true;
            }
            for (int i = 0; i < foo.Length-1; i++)
            {
                Console.Write(foo[i] + ", ");
            }
            Console.WriteLine(foo[29]);
        }
    }
}
