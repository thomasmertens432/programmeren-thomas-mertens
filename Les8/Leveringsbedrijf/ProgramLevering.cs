﻿ using System;

namespace Leveringsbedrijf
{
    class Program
    {
        static void Main(string[] args)
        {
            //int[] postcode = new int[10] {2020,2021,2022,2023,2024,2025,2026,2027,2028,2029};
            //int[] prijsPerKg = new int[10] {1, 2, 4, 8, 16, 32, 64, 128, 256, 512};

            //Console.WriteLine("Geef gewicht pakket");
            //int gewicht = Convert.ToInt32(Console.ReadLine());

            //Console.WriteLine("Naar welke postcode wenst u dit pakket te versturen?");
            //int postCode = Convert.ToInt32(Console.ReadLine());

            //if(postcode[)

            //Console.WriteLine("Dit zal" + " euro kosten.");

            string[] postcodes = { "2000", "2010", "2020", "2030", "2040", "2050", "2060", "2070", "2080", "2090" };
            double[] prijs = { 20, 32, 10, 25, 25.5, 22, 29, 49, 14.3, 21 };

            Console.WriteLine("Geef gewicht pakket");
            double gewicht = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Naar welke postcode wenst u dit pakket te versturen?");
            string keuze = Console.ReadLine();
            bool flag = false;
            for (int i = 0; i < postcodes.Length; i++)
            {

                if (postcodes[i] == keuze)
                {
                    double totaal = gewicht * prijs[i];
                    Console.WriteLine("Dit zal " + totaal + " euro kosten.");
                    flag = true;
                }
            }
            if (flag == false)
            {
                Console.WriteLine("Deze postcode werd niet in ons systeem teruggevonden.");
            }
        }
    }
}
