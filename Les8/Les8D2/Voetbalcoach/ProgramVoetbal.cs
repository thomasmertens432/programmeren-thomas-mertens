﻿using System;

namespace Voetbalcoach
{
    class Program
    {
        static void Main(string[] args)
        {

            int[,] actieSpeler = new int[12,2];
            bool flag = true;

            while (flag == true){
                Console.WriteLine("Geef een rugnummer in: ");
                int rugnummer = Convert.ToInt32(Console.ReadLine());
                if (rugnummer != 99)
                {
                    Console.WriteLine("Geef a voor een goede actie of b voor een domme actie: ");
                    char actie = Convert.ToChar(Console.ReadLine());

                    Console.WriteLine("Hoeveel acties wilt u doen? ");
                    int aantalActies = Convert.ToInt32(Console.ReadLine());

                    if (actie == 'a')
                    {
                        actieSpeler[rugnummer, 0] = aantalActies;
                    }
                    else
                    {
                        actieSpeler[rugnummer, 1] = aantalActies;
                    }
                }
                else
                {
                    flag = false;
                }
            }

            Console.WriteLine("Rugnummer\tGoede\tDomme\tVerschil");
            int biggest = int.MinValue, smallest = int.MaxValue, best = 0, worst = 0, average = 0; 
            for(int i=0; i < actieSpeler.GetLength(0); i++)
            {
                int verschil = actieSpeler[i, 0] - actieSpeler[i, 1];
                Console.WriteLine(i+1+"\t\t"+actieSpeler[i,0]+"\t"+ actieSpeler[i, 1] + "\t"+verschil);

                if (verschil > biggest)
                {
                    biggest = verschil;
                    best = i + 1;
                }
                
                if (verschil < smallest)
                {
                    smallest = verschil;
                    worst = i + 1;
                }

                if (verschil == 0 && average == 0)
                {
                    average = i + 1;
                }
            }

            Console.WriteLine("Beste speler: " + best + ", met als resultaat: " + biggest);
            Console.WriteLine("Slechtste speler: " + worst + ", met als resultaat: " + smallest);
            Console.WriteLine("Meest gemiddelde speler: speler " + average);
        }
    }
}
