﻿using System;
using System.Linq;

namespace Parkeergarage
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Geef aantal auto's in");
            int aantalAutos = Convert.ToInt32(Console.ReadLine());
            double[] pTijd = new double[aantalAutos];

            int g = 1;
            for (int i=0; i<aantalAutos; i++)
            {
                
                Console.WriteLine("Geef parkeertijd auto " + g + " in (uren):");
                pTijd[i] = Convert.ToDouble(Console.ReadLine());
                g++;
            }

            double[] kost = new double[aantalAutos];
            int b = 1;
            Console.WriteLine("Auto\tDuur\tKost");
            for (int j=0; j<pTijd.Length; j++) 
            {
                if (pTijd[j] < 3)
                {
                    kost[j] = 2;
                }
                else if (pTijd[j] > 20)
                {
                    kost[j] = 10;
                }
                else
                {
                    kost[j] = 2+((pTijd[j]-3) * 0.5);
                }
                
                Console.WriteLine(b + "\t" + pTijd[j] + "\t" + kost[j]);
                b++;
            }

            

            Console.WriteLine("Totaal\t"+pTijd.Sum() + "\t" + kost.Sum());
            
        }
    }
}
