﻿using System;

namespace _2d_Array_Viewer
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] array = { { 15, 6, 9 }, { 1, 2, 3 }, { 6, 9, 12 } };
            VisualiseerArray(array);
        }

        static void VisualiseerArray(int[,] array)
        {
            for(int i=0; i<3; i++){
                for(int j=0; j<3; j++)
                {
                    Console.Write(array[i, j]+"\t");
                }
                Console.WriteLine();
            }
        }
    }
}
