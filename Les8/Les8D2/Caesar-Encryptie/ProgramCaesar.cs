﻿using System;

namespace Caesar_Encryptie
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Tekst om te ontcijferen: ");
            string text = Console.ReadLine();

            Console.WriteLine("Aantal om te verschuiven: ");
            int aantal = Convert.ToInt32(Console.ReadLine());


            string encryptedText = Encrypt(text, aantal);
            Console.WriteLine(encryptedText);

            string decryptedText = Decrypt(encryptedText, aantal);
            Console.WriteLine(decryptedText);
        }

        static string Encrypt(string text, int key)
        {
            char[] characters = text.ToCharArray();
            string encrypted = "";

            foreach (char character in characters)
            {
                char nieuweLetter = character;
                if (character != ' ')
                {
                    nieuweLetter = (char)(character + key);
                }
                encrypted += nieuweLetter;
            }

            return encrypted;
        }

        static string Decrypt(string text, int key)
        {
            return Encrypt(text, 0 - key);
        }
    }
    
}
