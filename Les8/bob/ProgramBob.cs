﻿using System;

namespace bob
{
    class Program
    {
        static void Main(string[] args)
        {
            bool chat = true;
            string[] Bob = { "Sure.", "Whoa, chill out!", "Calm down, I know what I'm doing!", "Fine. Be that way!", "Whatever." };
            Console.WriteLine("Start de chat door iets te typen.");

            do
            {
                string person = Console.ReadLine().ToUpper();

                if (person.Contains("?") && person.Contains("!")) 
                {
                    Console.WriteLine(Bob[2]);
                }
                else if (person.Contains("!"))
                {
                    Console.WriteLine(Bob[1]);
                }
                else if (person.Contains("?")) 
                {
                    Console.WriteLine(Bob[0]);
                } 
                else if (person.Contains("BOB"))
                {
                    Console.WriteLine(Bob[3]);
                } 
                else
                {
                    Console.WriteLine(Bob[4]);
                }

            } while (chat == true);
        }
    }
}
