﻿using System;

namespace schoenverkoper
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Vanaf welk aantal geld de korting? ");
            int aantal = Convert.ToInt32(Console.ReadLine());   

            Console.Write("Hoeveel paar schoenen wil je kopen? ");
            int schoenen = Convert.ToInt32(Console.ReadLine());

            int prijs;
            if (schoenen < aantal)
            {
                prijs = 20;
            }
            else
            {
                prijs = 10;
            }

            int totaal = prijs * schoenen;
            Console.WriteLine("Je moet " + totaal + " euro betalen.");

        }
    }
}
