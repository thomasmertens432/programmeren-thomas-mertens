﻿using System;

namespace Ohm_Berekenaar
{
    class Program
    {
        static void Main(string[] args)
        {
            Boolean b = true;

            do
            {
                Console.WriteLine("Wat wil je berekenen? spanning, weerstand of stroomsterkte?");
                String keuze = Console.ReadLine();
                double spanning, weerstand, stroomsterkte;

                if (keuze == "spanning")
                {
                    Console.Write("Wat is de weerstand? ");
                    weerstand = Convert.ToDouble(Console.ReadLine());
                    Console.Write("Wat is de stroomsterkte? ");
                    stroomsterkte = Convert.ToDouble(Console.ReadLine());

                    spanning = weerstand * stroomsterkte;
                    Console.WriteLine("De spanning bedraagt: " + spanning);
                    b = true;
                }
                else if (keuze == "weerstand")
                {
                    Console.Write("Wat is de spanning? ");
                    spanning = Convert.ToDouble(Console.ReadLine());
                    Console.Write("Wat is de stroomsterkte? ");
                    stroomsterkte = Convert.ToDouble(Console.ReadLine());

                    weerstand = spanning / stroomsterkte;
                    Console.WriteLine("De weerstand bedraagt: " + weerstand);
                    b = true;
                }
                else if (keuze == "stroomsterkte")
                {
                    Console.Write("Wat is de weerstand? ");
                    weerstand = Convert.ToDouble(Console.ReadLine());
                    Console.Write("Wat is de spanning? ");
                    spanning = Convert.ToDouble(Console.ReadLine());

                    stroomsterkte = spanning / weerstand;
                    Console.WriteLine("De stroomsterkte bedraagt: " + stroomsterkte);
                    b = true;
                }
                else
                {
                    Console.WriteLine("Je hebt iets ongebruikt ingetypt. Probeer opnieuw.");
                    b=false;
                }
            } while (b == false);

        }
    }
}
