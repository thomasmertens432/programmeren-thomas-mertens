﻿using System;

namespace schrikkeljaar
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("> ");
            int schrikkeljaar = Convert.ToInt32(Console.ReadLine());

            if(schrikkeljaar%4==0 && schrikkeljaar%100!=0)
            {
                Console.WriteLine("Schrikkeljaar");
            } else if (schrikkeljaar%4==0 && schrikkeljaar % 100 == 0 && schrikkeljaar % 400 == 0)
            {
                Console.WriteLine("Schrikkeljaar");
            } else
            {
                Console.WriteLine("Geen schrikkeljaar");
            }
        }
    }
}
