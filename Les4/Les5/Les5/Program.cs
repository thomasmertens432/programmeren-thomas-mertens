﻿using System;

namespace Les5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hoe veel weeg je in kg?");
            double gewicht = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Hoe groot ben je in m?");
            double grootte = Convert.ToDouble(Console.ReadLine());

            double BMI = gewicht / (grootte * grootte);
            BMI = Math.Round(BMI, 2, MidpointRounding.AwayFromZero);

            Console.WriteLine("Je BMI bedraagt " + BMI + ".");

            if (BMI < 18.5)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Ondergewicht");
            }
            else if (BMI < 25)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Normaal gewicht");
            }
            else if (BMI < 30)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Overgewicht");
            }
            else if (BMI < 40)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Zwaarlijvig");
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.WriteLine("Ernstige obesitas");
            }
        }
    }
}
