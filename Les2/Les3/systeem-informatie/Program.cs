﻿using System;

namespace systeem_informatie
{
    class Program
    {
        static void Main(string[] args)
        {
            bool is64Bit = Environment.Is64BitOperatingSystem;
            string pcNaam = Environment.MachineName;
            int procCount = Environment.ProcessorCount;
            string userName = Environment.UserName;
            long memory = Environment.WorkingSet;

            Console.WriteLine($"Uw computer heeft een 64-bit besturingssysteem: {is64Bit}");
            Console.WriteLine($"De naam van uw PC is: {pcNaam}");
            Console.WriteLine($"Uw pc heeft {procCount} processorkernen.");
            Console.WriteLine($"{userName} is uw gebruikersnaam");
            Console.WriteLine($"Je gebruikt {memory} megabytes aan geheugen.");
        }
    }
}
