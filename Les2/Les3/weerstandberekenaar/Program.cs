﻿using System;

namespace weerstandberekenaar
{
    class Program
    {
        static void Main(string[] args)
        {
            int userRing1, userRing2, userRing3;

            Console.WriteLine("Geef de waarde (uitgedrukt in een getal van 0 tot 9) van de eerste ring:");
            userRing1 = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Geef de waarde (uitgedrukt in een getal van 0 tot 9) van de tweede ring:");
            userRing2 = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Geef de waarde(uitgedrukt in een getal van - 2 tot 7) van de derde ring(exponent):");
            userRing3 = Convert.ToInt32(Console.ReadLine());

            string resultaatRingen = ($"Resultaat is {((userRing1 * 10 + userRing2) * Math.Pow(10, userRing3))} Ohm, ofwel {userRing1 * 10 + userRing2}x{Math.Pow(10, userRing3)}");
            string Ohm = ($"{((userRing1 * 10 + userRing2) * Math.Pow(10, userRing3))} Ohm");


            Console.WriteLine("╔═══════╦═══════╦═══════╦═════════════╗");
            Console.WriteLine("║ ring1 ║ ring2 ║ ring3 ║ Totaal(Ohm) ║");
            Console.WriteLine("╟───────╫───────╫───────╫─────────────╢");
            Console.WriteLine("║"+ userRing1 + "      ║" +userRing2 + "      ║" + userRing3 + "      ║" + Ohm+ "     ║");
            Console.WriteLine("╚═══════╩═══════╩═══════╩═════════════╝");
        }
    }
}
