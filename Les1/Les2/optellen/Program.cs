﻿using System;

namespace optellen
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Geef het eerste getal");
            int eerste = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Geef het tweede getal");
            int tweede = Convert.ToInt32(Console.ReadLine());

            int uitkomst = eerste + tweede;
            Console.WriteLine("De som is: "+uitkomst);
        }
    }
}
