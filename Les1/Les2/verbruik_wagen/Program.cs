﻿using System;

namespace verbruik_wagen
{
    class Program
    {
        static void Main(string[] args)
        {
            double litervoor, literna, kilometervoor, kilometerna;

            Console.Write("Geef het aantal liter in de tank voor de rit: ");
            litervoor = Convert.ToDouble(Console.ReadLine());

            Console.Write("Geef het aantal liter in de tank na de rit: ");
            literna = Convert.ToDouble(Console.ReadLine());

            Console.Write("Geef de kilometerstand voor de rit: ");
            kilometervoor = Convert.ToDouble(Console.ReadLine());

            Console.Write("Geef de kilometerstand na de rit: ");
            kilometerna = Convert.ToDouble(Console.ReadLine());


            double verbruik = (100 * (litervoor - literna) / (kilometerna - kilometervoor));


            Console.WriteLine("Het verbruik van de auto is: " + verbruik);

            double afgerond = Math.Round(verbruik, 2);
            Console.Write("Het afgeronde verbruik van de auto is: " + afgerond);
        }
    }
}
