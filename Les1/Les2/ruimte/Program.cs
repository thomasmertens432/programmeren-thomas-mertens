﻿using System;

namespace ruimte
{
    class Program
    {
        static void Main(string[] args)
        {
            int gewicht = 100;
            double Mercurius = gewicht * 0.38;
            double Venus = gewicht *0.91;
            double Aarde = gewicht;
            double Mars = gewicht * 0.38;
            double Jupiter = gewicht * 2.34;
            double Saturnus = gewicht * 1.06;
            double Uranus = gewicht * 0.92;
            double Neptunus = gewicht * 1.19;
            double Pluto = gewicht * 0.06;
            
            Console.WriteLine("Op Mercurius heb je een schijnbaar gewicht van " +Mercurius+"kg.");
            Console.WriteLine("Op Venus heb je een schijnbaar gewicht van " + Venus+"kg.");
            Console.WriteLine("Op Aarde heb je een schijnbaar gewicht van " + Aarde+"kg.");
            Console.WriteLine("Op Mars heb je een schijnbaar gewicht van " + Mars+"kg.");
            Console.WriteLine("Op Jupiter heb je een schijnbaar gewicht van " + Jupiter+"kg.");
            Console.WriteLine("Op Saturnus heb je een schijnbaar gewicht van " + Saturnus+"kg.");
            Console.WriteLine("Op Uranus heb je een schijnbaar gewicht van " + Uranus+"kg.");
            Console.WriteLine("Op Neptunus heb je een schijnbaar gewicht van " + Neptunus+"kg.");
            Console.WriteLine("Op Pluto heb je een schijnbaar gewicht van " + Pluto+"kg.");
        }
    }
}
