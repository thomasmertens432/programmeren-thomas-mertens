﻿using System;

namespace veel_kleintjes
{
    class Program
    {
        static int Largest(int getal1, int getal2)
        {
            if(getal1 > getal2)
            {
                return getal1;
            }
            else
            {
                return getal2;
            }
        }

        static void IsEven(int getal)
        {
            if (getal % 2 == 0)
            {
                Console.WriteLine("Het getal is even");
            }
            else
            {
                Console.WriteLine("Het getal is oneven");
            }
        }


        static void Main(string[] args)
        {
            //Largest
            Console.WriteLine("Welke twee getallen wil je vergelijken?");
            int getal1 = Convert.ToInt32(Console.ReadLine());
            int getal2 = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine(Largest(getal1,getal2) + "is het grootste getal");

            //IsEven
            Console.Write("Geef een getal en ik zeg of het even is: ");
            int evenoneven = Convert.ToInt32(Console.ReadLine());

            IsEven(evenoneven);
            Console.WriteLine();
        }
    }
}
