﻿using System;

namespace voorstellen_plus
{
    class Program
    {
        static void MyIntro(String Naam, int leeftijd, String adres)
        {
            Console.WriteLine("Ik ben "+Naam + ", ik ben " + leeftijd + " jaar en woon in de " + adres);
        }

        static void Main(string[] args)
        {
            MyIntro("Thomas Mertens", 18, "Honingstraat 10");
        }
    }
}
