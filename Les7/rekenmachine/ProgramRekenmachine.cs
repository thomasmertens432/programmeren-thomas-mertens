﻿using System;

namespace rekenmachine
{
    class Program
    {
        static double TelOp(int getal1, int getal2)
        {
            double uitkomst = getal1 + getal2;
            return uitkomst;
        }

        static double TrekAf(int getal1, int getal2)
        {
            double uitkomst = getal1 - getal2;
            return uitkomst;
        }

        static double VermenigVuldig(int getal1, int getal2)
        {
            double uitkomst = getal1 * getal2;
            return uitkomst;
        }

        static double Deel(int getal1, int getal2)
        {
            double uitkomst = getal1 / getal2;
            return uitkomst;
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Maak een keuze uit");
            Console.WriteLine("1. Optellen");
            Console.WriteLine("2. Aftrekken");
            Console.WriteLine("3. Vermenigvuldigen");
            Console.WriteLine("4. Delen");
            Console.Write("Geef het overeenstemmende nummer als keuze in: ");
            int keuze = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine();
            Console.Write("Geef het eerste nummer in: ");
            int getal1 = Convert.ToInt32(Console.ReadLine());
            Console.Write("Geef het tweede nummer in: ");
            int getal2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine();
            switch (keuze)
            {
                case 1:
                    Console.WriteLine("Resultaat: (" + getal1 + " + " + getal2 + ") = " + TelOp(getal1,getal2));
                    break;

                case 2:
                    Console.WriteLine("Resultaat: (" + getal1 + " - " + getal2 + ") = " + TrekAf(getal1, getal2));
                    break;

                case 3:
                    Console.WriteLine("Resultaat: (" + getal1 + " * " + getal2 + ") = " + VermenigVuldig(getal1, getal2));
                    break;

                case 4:
                    Console.WriteLine("Resultaat: (" + getal1 + " / " + getal2 + ") = " + Deel(getal1, getal2));
                    break;
            }

        }
    }
}
