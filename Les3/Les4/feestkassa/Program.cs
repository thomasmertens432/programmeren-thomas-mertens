﻿using System;

namespace feestkassa
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Mosselen met frietjes?");
            int mosselfriet = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Koninginnenhapjes?");
            int hapjes = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Ijsjes?");
            int ijsjes = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Dranken?");
            int dranken = Convert.ToInt32(Console.ReadLine());

            int prijsmosselfriet = mosselfriet * 20;
            int prijshapjes = hapjes * 10;
            int prijsijsjes = ijsjes * 3;
            int prijsdranken = dranken * 2;
            int totaal = prijsdranken + prijshapjes + prijsijsjes + prijsmosselfriet;

            Console.WriteLine("Het totaal te betalen bedrag is " + totaal + " EURO");
        }
    }
}
