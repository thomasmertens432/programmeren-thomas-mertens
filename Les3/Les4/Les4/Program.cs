﻿using System;

namespace Les4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Geef een hoek, uitgedrukt in graden.");
            int graden = Convert.ToInt32(Console.ReadLine());

            double radialen = graden * (Math.PI / 180);
            radialen = Math.Round(radialen, 2);

            double sinus = Math.Sin(radialen);
            double cos = Math.Cos(radialen);
            double tan = Math.Tan(radialen);
            sinus = Math.Round(sinus, 2, MidpointRounding.AwayFromZero);
            cos = Math.Round(cos, 2, MidpointRounding.AwayFromZero);
            tan = Math.Round(tan, 2, MidpointRounding.AwayFromZero);

            Console.WriteLine(graden + " graden is " + radialen + " radialen.");
            Console.WriteLine("De sinus is " + sinus);
            Console.WriteLine("De cosinus is " + cos);
            Console.WriteLine("De tangens is " + tan);
        }
    }
}
