﻿using System;

namespace random_invoer
{
    class Program
    {
        static void Main(string[] args)
        {
            Random random = new Random();

            Console.WriteLine("Voer het bedrag in?");
            int bedrag = random.Next(1, 50);
            Console.WriteLine("Automatisch gegenereerd: " + bedrag);
            Console.WriteLine("De poef staat op " + bedrag);
            Console.ReadLine();

            Console.WriteLine("Voer het bedrag in?");
            int bedrag2 = random.Next(1, 50);
            Console.WriteLine("Automatisch gegenereerd: " + bedrag2);
            bedrag += bedrag2;
            Console.WriteLine("De poef staat op " + bedrag);
            Console.ReadLine();

            Console.WriteLine("Voer het bedrag in?");
            bedrag2 = random.Next(1, 50);
            Console.WriteLine("Automatisch gegenereerd: " + bedrag2);
            bedrag += bedrag2;
            Console.WriteLine("De poef staat op " + bedrag);
            Console.ReadLine();

            Console.WriteLine("Voer het bedrag in?");
            bedrag2 = random.Next(1, 50);
            Console.WriteLine("Automatisch gegenereerd: " + bedrag2);
            bedrag += bedrag2;
            Console.WriteLine("De poef staat op " + bedrag);
            Console.ReadLine();

            Console.WriteLine("Voer het bedrag in?");
            bedrag2 = random.Next(1, 50);
            Console.WriteLine("Automatisch gegenereerd: " + bedrag2); 
            bedrag += bedrag2;
            Console.WriteLine("De poef staat op " + bedrag);
            Console.ReadLine();

            Console.WriteLine("*************************");
            Console.WriteLine("Het totaal van de poef is " + bedrag + " euro");
        }
    }
}
