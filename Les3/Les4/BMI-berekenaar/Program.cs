﻿using System;

namespace BMI_berekenaar
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hoe veel weeg je in kg?");
            double gewicht = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Hoe groot ben je in m?");
            double grootte = Convert.ToDouble(Console.ReadLine());

            double BMI = gewicht / (grootte*grootte);
            BMI = Math.Round(BMI, 2, MidpointRounding.AwayFromZero);
            
            Console.WriteLine("Je BMI bedraagt "+ BMI+ ".");

            
        }
    }
}
