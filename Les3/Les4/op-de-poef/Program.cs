﻿using System;

namespace op_de_poef
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Voer het bedrag in?");
            int bedrag = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("De poef staat op " + bedrag);

            Console.WriteLine("Voer het bedrag in?");
            bedrag += Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("De poef staat op " + bedrag);

            Console.WriteLine("Voer het bedrag in?");
            bedrag += Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("De poef staat op " + bedrag);

            Console.WriteLine("Voer het bedrag in?");
            bedrag += Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("De poef staat op " + bedrag);

            Console.WriteLine("Voer het bedrag in?");
            bedrag += Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("De poef staat op " + bedrag);

            Console.WriteLine("*************************");
            Console.WriteLine("Het totaal van de poef is " + bedrag + " euro");


            double afbetalingen = (double)bedrag / 10;
            Console.WriteLine(Math.Ceiling(afbetalingen));
            Console.WriteLine("Dit zal " + afbetalingen+ " afbetalingen vragen.");

        }
    }
}
